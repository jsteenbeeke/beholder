package com.jeroensteenbeeke.topiroll.beholder.dao;

import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.topiroll.beholder.entities.DungeonMasterNote;
import com.jeroensteenbeeke.topiroll.beholder.entities.filter.DungeonMasterNoteFilter;

public interface DungeonMasterNoteDAO extends DAO<DungeonMasterNote, DungeonMasterNoteFilter> {}
