package com.jeroensteenbeeke.topiroll.beholder.dao;

import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.topiroll.beholder.entities.SessionLogIndex;
import com.jeroensteenbeeke.topiroll.beholder.entities.filter.SessionLogIndexFilter;

public interface SessionLogIndexDAO extends DAO<SessionLogIndex,SessionLogIndexFilter> {

}
