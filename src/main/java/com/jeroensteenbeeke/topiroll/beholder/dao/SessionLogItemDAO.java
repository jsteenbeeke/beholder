package com.jeroensteenbeeke.topiroll.beholder.dao;

import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.topiroll.beholder.entities.SessionLogItem;
import com.jeroensteenbeeke.topiroll.beholder.entities.filter.SessionLogItemFilter;

public interface SessionLogItemDAO extends DAO<SessionLogItem,SessionLogItemFilter> {

}
